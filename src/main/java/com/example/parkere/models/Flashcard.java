package com.example.parkere.models;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Flashcard implements Serializable {
    private String word;
    private ArrayList<String> translations;
    private String public_comment;
    private String private_comment;
    private Language learningLanguage;
    private int score;
    private History history;
    @Serial
    private static final long serialVersionUID = 1415005;
    // String context;

    public Flashcard(String word, ArrayList<String> translations) {
        this.word = word;
        this.translations = translations;
        this.public_comment = "";
        this.private_comment = "";
        this.history = new History();
        this.score = 0;
    }

    public Flashcard(String word, ArrayList<String> translations, String public_comment) {
        this.word = word;
        this.translations = translations;
        this.public_comment = public_comment;
        this.private_comment = "";
        this.history = new History();
        this.score = 0;
    }

    public Flashcard(String word, ArrayList<String> translations, String public_comment, String private_comment) {
        this.word = word;
        this.translations = translations;
        this.public_comment = public_comment;
        this.private_comment = private_comment;
    }

    @Override
    public String toString() {
        String result = "[" + word + " - ";
        for (int i = 0; i < translations.size(); ++i) {
            result = result + translations.get(i);
            if (i < translations.size() - 1) result = result + ", ";
        }
        return result + " (" + score + ")]";
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<String> getTranslations() {
        return translations;
    }

    public void setTranslations(ArrayList<String> translations) {
        this.translations = translations;
    }

    public String getPublic_comment() {
        return public_comment;
    }

    public void setPublic_comment(String public_comment) {
        this.public_comment = public_comment;
    }

    public String getPrivate_comment() {
        return private_comment;
    }

    public void setPrivate_comment(String private_comment) {
        this.private_comment = private_comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setLearningLanguage(Language learningLanguage) {
        this.learningLanguage = learningLanguage;
    }

    public Language getLearningLanguage() {
        return learningLanguage;
    }

    public void addToHistory(HistoryRecord historyRecord) {
        this.history.addRecord(historyRecord);
    }

    public int getHistorySize() {
        return this.history.size();
    }

    protected History getHistory() {
        return this.history;
    }
}

class CompareFlashcards implements Comparator<Flashcard>, Serializable {

    @Override
    public int compare(Flashcard o1, Flashcard o2) {
        return o1.getScore() - o2.getScore();
    }
    @Serial private static final long serialVersionUID = 87715520;
}