package com.example.parkere.models;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.PriorityQueue;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class FlashcardList implements Collection<Flashcard>, Serializable {
    PriorityQueue<Flashcard> list;
    String name;
    String description;
    Language teachingLanguage;
    Language learningLanguage;
    String pathname;
    @Serial private static final long serialVersionUID = 5465312;

    public FlashcardList(String name) {
        list = new PriorityQueue<>(1, new CompareFlashcards());
        this.name = name;
        pathname = "src/data/sets/" + this.name;
    }

    public FlashcardList(String name, int n) {
        list = new PriorityQueue<>(n, new CompareFlashcards());
        this.name = name;
        pathname = "src/data/sets/" + this.name;
    }

    public FlashcardList(String name, File file) throws FileNotFoundException {
        list = new PriorityQueue<>(1, new CompareFlashcards());
        Importer importer = new Importer();
        addAll(importer.importAll(file));
        this.name = name;
        pathname = "src/data/sets/" + this.name;
    }

    public FlashcardList(FileInputStream file) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(file);
        FlashcardList flashcardList = (FlashcardList) in.readObject();
        this.list = flashcardList.list;
        this.name = flashcardList.name;
        this.description = flashcardList.description;
        this.teachingLanguage = flashcardList.teachingLanguage;
        this.learningLanguage = flashcardList.learningLanguage;
        this.pathname = flashcardList.pathname;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<Flashcard> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(Flashcard flashcard) {
        return list.add(flashcard);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Flashcard> c) {
        return list.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public String toString() {
        return "FlashcardList{\n" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", teachingLanguage='" + teachingLanguage + '\'' +
                ", learningLanguage='" + learningLanguage + '\'' +
                ", \nlist=" + list +
                "\n}";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getTeachingLanguage() {
        return teachingLanguage;
    }

    public void setTeachingLanguage(Language teachingLanguage) {
        this.teachingLanguage = teachingLanguage;
    }

    public Language getLearningLanguage() {
        return learningLanguage;
    }

    public void setLearningLanguage(Language learningLanguage) {
        this.learningLanguage = learningLanguage;
        for (Flashcard f : list) f.setLearningLanguage(learningLanguage);
    }

    public Flashcard getAndRemoveFirst() {
        return list.remove();
    }

    public void save() throws IOException {
        FileOutputStream file = new FileOutputStream(pathname);
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(this);
    }

    public int getSize() {
        return list.size();
    }

    public void setPathname(String pathname) {
        this.pathname = pathname;
    }

    public void refresh() {
        PriorityQueue<Flashcard> temp = list;
        list.clear();
        while (!temp.isEmpty()) {
            Flashcard f = temp.remove();
            History h = f.getHistory();
            long timeDifference = System.currentTimeMillis() - h.lastTimestamp();
            double successRate = h.successRate();
            int recentOK = h.countRecentResults(ResultType.OK, min(5, h.size()));
            int currentScore = f.getScore();
            currentScore -= (int)(timeDifference/((double)1000*60*60*24))*(20/(1+recentOK*successRate));
            f.setScore(max(currentScore, 0));
        }
    }
}