package com.example.parkere.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Importer {
   Flashcard importOne(String line) {
       String[] s = line.split("\\t");
       String[] translations = s[1].split(", *");
       if (s.length <= 2)
           return new Flashcard(s[0], new ArrayList<>(List.of(translations)));
       else if (s.length == 3)
           return new Flashcard(s[0], new ArrayList<>(List.of(translations)), s[2]);
       else
           return new Flashcard(s[0], new ArrayList<>(List.of(translations)), s[2], s[3]);
   }

   ArrayList<Flashcard> importAll(File file) throws FileNotFoundException {
       ArrayList<Flashcard> flashcardList = new ArrayList<>();
       Scanner sc = new Scanner(file);
       while (sc.hasNextLine()) {
           flashcardList.add(importOne(sc.nextLine()));
       }
       return flashcardList;
   }
}
