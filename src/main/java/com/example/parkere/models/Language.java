package com.example.parkere.models;

import java.io.Serial;
import java.io.Serializable;

public class Language implements Serializable {
    String name;
    String code;
    @Serial private static final long serialVersionUID = 200034511;

    public Language(String name) {
        this.name = name;
        this.code = name;
    }

    public Language(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return name;
    }
}
