package com.example.parkere.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class Context {
    Flashcard flashcard;
    ArrayList<String> occurrences;
    File default_folder;

    final int MAX_LINE_LENGTH = 250;

    public Context(Flashcard flashcard) throws IOException {
        this.flashcard = flashcard;
        occurrences = new ArrayList<>();
        default_folder = new File("src/data/contexts/" + flashcard.getLearningLanguage().getCode());
        findText(null);
    }

    private void findText(File folder) throws IOException {
        if (folder == null) folder = default_folder;
        if (folder.listFiles() == null) return;
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                findText(fileEntry);
            } else {
                searchAFile(fileEntry);
            }
        }
    }

    private void searchAFile(File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.toLowerCase().contains(flashcard.getWord().toLowerCase())) {
                if (line.length() < MAX_LINE_LENGTH) occurrences.add(line);
                /*String [] sentences = line.split(".");
                if (sentences.length > 0) System.out.println(sentences[0]);
                for (String s : sentences) {
                    System.out.println(s);
                    if (s.toLowerCase().contains(flashcard.getWord().toLowerCase())) occurrences.add(s);
                }*/
            }
        }
    }

    public String returnContext() {
        int n = occurrences.size();
        if (n == 0) return "";
        Random rand = new Random();
        return occurrences.get(rand.nextInt(n));
    }
}
