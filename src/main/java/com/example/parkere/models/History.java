package com.example.parkere.models;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;

class HistoryRecord implements Serializable {
    ResultType resultType;
    QuestionType questionType;
    long timestamp;
    @Serial private static final long serialVersionUID = 711656544;

    public HistoryRecord(ResultType resultType, QuestionType questionType, long timestamp) {
        this.resultType = resultType;
        this.questionType = questionType;
        this.timestamp = timestamp;
    }
}

public class History implements Serializable {
    ArrayList<HistoryRecord> records = new ArrayList<>();
    @Serial private static final long serialVersionUID = 70707254;

    int countResults(ResultType resultType) {
        int result = 0;
        for (HistoryRecord historyRecord: records) {
            if (historyRecord.resultType == resultType) result++;
        }
        return result;
    }

    int countRecentResults(ResultType resultType, int m) {
        int result = 0;
        int n = records.size();
        for (int i = n - m; i < n; ++i) {
            if (records.get(i).resultType == resultType) result++;
        }
        return result;
    }

    double successRate() {
        int ok_no = countResults(ResultType.OK);
        int almost_no = countResults(ResultType.ALMOST);
        return ((double)ok_no + 0.5*(double)almost_no)/((double) records.size());
    }

    void addRecord(HistoryRecord historyRecord) {
        records.add(historyRecord);
    }

    int size() {
        return records.size();
    }

    long lastTimestamp() {
        return records.get(records.size() - 1).timestamp;
    }
}
