package com.example.parkere.models;

public enum ResultType {
    OK, ALMOST, WRONG
}