package com.example.parkere;

import com.example.parkere.models.FlashcardList;
import com.example.parkere.models.Language;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static com.example.parkere.AlertBoxController.displayAlert;

public class ImportFromCSVController {

    private FlashcardList importedSet;
    private File chosenFile;

    @FXML
    Button chooseFileButton;
    @FXML
    Button saveButton;
    @FXML
    TextField setNameField, setDescriptionField, learningLanguageField, translationsLanguageField;
    @FXML
    ChoiceBox<String> choiceBox = new ChoiceBox<>();
    private String selectedAlgorithm;

    @FXML
    public void initialize() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        chooseFileButton.setOnAction(event -> {
            chosenFile = fileChooser.showOpenDialog(null);
            try {
                handleChosenFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void goToPreviousScene(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateSetScene.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void saveImportedSet() throws IOException {
        String setName = setNameField.getText();
        if (Objects.equals(setName, "")) {
            displayAlert("You have to name your set.");
            return;
        }
        if (learningLanguageField.getText().equals("") || translationsLanguageField.getText().equals("")) {
            displayAlert("You didn't choose language " +
                    ((learningLanguageField.getText().equals("")) ? "to learn." : "of translations."));
            return;
        }
        try {
            importedSet = new FlashcardList(setName, chosenFile);
        } catch (Exception e) {
            displayAlert("The file is in the wrong format or wasn't chosen at all.");
            return;
        }
        importedSet.setLearningLanguage(new Language(learningLanguageField.getText()));
        importedSet.setTeachingLanguage(new Language(translationsLanguageField.getText()));
        importedSet.setPathname("src/data/sets/" + setName);
        importedSet.setDescription(setDescriptionField.getText());
        importedSet.save();
        importedSet = null;
        chooseFileButton.setText("Choose file");
        setNameField.setText("");
        setDescriptionField.setText("");
        learningLanguageField.setText("");
        translationsLanguageField.setText("");
    }

    private void handleChosenFile() throws IOException {
        if (chosenFile == null) {
            importedSet = null;
            chooseFileButton.setText("Choose file");
        } else {
            chooseFileButton.setText(chosenFile.getName());
        }
    }
}
