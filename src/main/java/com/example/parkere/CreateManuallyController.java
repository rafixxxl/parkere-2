package com.example.parkere;

import com.example.parkere.models.Flashcard;
import com.example.parkere.models.FlashcardList;
import com.example.parkere.models.Language;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.example.parkere.AlertBoxController.displayAlert;

public class CreateManuallyController {

    @FXML TextField word1Field, word2Field, language1Field, language2Field, setNameField, setDescriptionField;
    @FXML TableView<Pair<String, String>> tableView;
    @FXML TableColumn<Pair<String, String>, String> words;
    @FXML TableColumn<Pair<String, String>, String> translations;

    ObservableList<Pair<String, String>> flashcards;
    HashMap<String, String> futureSet = new HashMap<>();

    @FXML
    void initialize() {
        words.setCellValueFactory(new PropertyValueFactory<>("key"));
        translations.setCellValueFactory(new PropertyValueFactory<>("value"));
        flashcards = FXCollections.observableArrayList();
        tableView.setEditable(true);
        words.setCellFactory(TextFieldTableCell.forTableColumn());
        translations.setCellFactory(TextFieldTableCell.forTableColumn());
    }

    @FXML
    void addFlashCardToTable() throws IOException {
        String word1 = word1Field.getText();
        String word2 = word2Field.getText();
        if (word1.equals("") || word2.equals("")) {
            displayAlert("One of the fields is empty.");
            return;
        }
        if (futureSet.containsKey(word1)) {
            displayAlert("You already added this word.");
            return;
        }
        Pair<String, String> newPair = new Pair<>(word1, word2);
        flashcards.add(newPair);
        futureSet.put(word1, word2);
        tableView.setItems(flashcards);
        word1Field.setText("");
        word2Field.setText("");
    }

    @FXML
    public void changeFirstCellEvent(CellEditEvent editedCell) throws IOException {
        Pair<String, String> pairSelected = tableView.getSelectionModel().getSelectedItem();
        String oldWord = pairSelected.getKey();
        String newWord = editedCell.getNewValue().toString();
        if (futureSet.containsKey(newWord)) {
            displayAlert("You already added this word.");
            return;
        }
        String value = pairSelected.getValue();
        futureSet.remove(oldWord);
        futureSet.put(newWord, value);
    }

    @FXML
    public void changeSecondCellEvent(CellEditEvent editedCell) {
        Pair<String, String> pairSelected =  tableView.getSelectionModel().getSelectedItem();
        String oldWord = pairSelected.getValue();
        futureSet.put(oldWord, editedCell.getNewValue().toString());
    }

    @FXML
    public void deleteButtonPushed() {
        ObservableList<Pair<String, String>> selectedRows, allPairs;
        allPairs = tableView.getItems();
        selectedRows = tableView.getSelectionModel().getSelectedItems();
        for (Pair<String, String> pair: selectedRows) {
            futureSet.remove(pair.getKey());
            allPairs.remove(pair);
            if (allPairs.isEmpty()) return;
        }
    }

    @FXML
    void donePressed(ActionEvent event) throws IOException {
        Language lang1 = new Language(language1Field.getText(), language1Field.getText());
        Language lang2 = new Language(language2Field.getText(), language1Field.getText());
        if (language1Field.getText().equals("") || language2Field.getText().equals("")) {
            displayAlert("You didn't specify a language.");
            return;
        }
        if (setNameField.getText().equals("")) {
            displayAlert("You have to name your set.");
            return;
        }
        FlashcardList flashcardList = new FlashcardList(setNameField.getText());
        flashcardList.setDescription(setDescriptionField.getText());
        for (String key : futureSet.keySet()) {
            String[] temp = futureSet.get(key).split(", *");
            ArrayList<String> translations = new ArrayList<>(Arrays.asList(temp));
            flashcardList.add(new Flashcard(key, translations));
        }
        if (flashcardList.getSize() == 0) {
            displayAlert("You cannot create empty set.");
            return;
        }
        flashcardList.setPathname("src/data/sets/" + setNameField.getText());
        flashcardList.setLearningLanguage(lang1);
        flashcardList.setTeachingLanguage(lang2);
        flashcardList.save();
        goToPreviousScene(event);
    }

    @FXML
    void goToPreviousScene(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateSetScene.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}