package com.example.parkere;

import com.example.parkere.models.FlashcardList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

import static com.example.parkere.AlertBoxController.displayAlert;

public class ChooseSetController {

    private Parent root;
    private Scene scene;
    private Stage stage;
    private String selectedSet;

    @FXML
    ChoiceBox<String> setSelector;

    @FXML
    public void initialize() throws IOException {
        for (final File fileEntry : Objects.requireNonNull(new File("src/data/sets").listFiles())) {
            if (!fileEntry.isDirectory()) {
                String[] temp = fileEntry.getAbsolutePath().split("/");
                setSelector.getItems().add(temp[temp.length - 1]);
            }
        }
        setSelector.setOnAction(event -> selectedSet = setSelector.getValue());
    }

    @FXML
    void goToLearningScene(ActionEvent event) throws IOException, ClassNotFoundException {
        if (selectedSet == null) {
            displayAlert("You didn't choose a set.");
            return;
        }
        FXMLLoader loader = new FXMLLoader(getClass().getResource("LearningScene.fxml"));
        root = loader.load();

        LearningController learningController = loader.getController();
        learningController.prepareScene(new FlashcardList(new FileInputStream("src/data/sets/" + selectedSet)));

        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void goToMainScene(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MenuScene.fxml"));
        root = loader.load();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
