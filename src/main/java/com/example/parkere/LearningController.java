package com.example.parkere;

import com.example.parkere.models.Flashcard;
import com.example.parkere.models.FlashcardList;
import com.example.parkere.models.ResultType;
import com.example.parkere.models.ReviewSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class LearningController {

    private ReviewSession session;
    boolean showRevisionSettings;
    boolean alreadyAnswered;
    Flashcard currentFlashCard;

    @FXML
    Text flashCardText, resultText, translationsText;
    @FXML
    TextField answerField;

    @FXML
    void confirmAnswer() {
        String answer = answerField.getText().toLowerCase();
        printResultAndTranslations(session.validateType(answer));
    }

    @FXML
    void getNextFlashCard() throws IOException {
        alreadyAnswered = false;
        currentFlashCard = session.nextFlashcard();
        if (currentFlashCard == null) {
            session.end();
            session.restart(5);
            currentFlashCard = session.nextFlashcard();
        }
        if (currentFlashCard.getTranslations().size() == 1) {
            flashCardText.setText(currentFlashCard.getTranslations().get(0));
        } else {
            StringBuilder toPrint = new StringBuilder();
            for (var translation : currentFlashCard.getTranslations()) {
                toPrint.append(translation).append(", ");
            }
            toPrint = new StringBuilder(toPrint.substring(0, toPrint.length() - 2));
            flashCardText.setText(toPrint.toString());
        }
        answerField.setText("");
        resultText.setText("");
        translationsText.setText("");
    }

    @FXML
    void goToMainScene(ActionEvent event) throws IOException {
        endSession();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ChooseSetScene.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void prepareScene(FlashcardList flashcardList) throws IOException {
        alreadyAnswered = false;
        showRevisionSettings = true;
        resultText.setText("");
        translationsText.setText("");
        session = new ReviewSession(5, flashcardList);
        currentFlashCard = session.nextFlashcard();
        if (currentFlashCard.getTranslations().size() == 1) {
            flashCardText.setText(currentFlashCard.getTranslations().get(0));
        } else {
            StringBuilder toPrint = new StringBuilder();
            for (var translation : currentFlashCard.getTranslations()) {
                toPrint.append(translation).append(", ");
            }
            toPrint = new StringBuilder(toPrint.substring(0, toPrint.length() - 2));
            flashCardText.setText(toPrint.toString());
        }
    }

    private void endSession() throws IOException {
        session.end();
    }

    private void printResultAndTranslations(ResultType resultType) {
        if (resultType == ResultType.OK) resultText.setText("\uD83D\uDDF8");
        if (resultType == ResultType.OK) {
            resultText.setFill(Paint.valueOf("GREEN"));
            return;
        }
        if (resultType == ResultType.WRONG) resultText.setText("✗");
        resultText.setFill(Paint.valueOf("RED"));
        translationsText.setText("Correct answer: " + currentFlashCard.getWord());
    }
}
