package com.example.parkere;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class AlertBoxController {

    @FXML Button closeButton;
    @FXML Text errorMessage;

    @FXML
    void closeButton() {
        Stage alertBox = (Stage) closeButton.getScene().getWindow();
        alertBox.close();
    }

    public void setErrorMessage(String message) {
        errorMessage.setText(message);
    }

    public static void displayAlert(String msg) throws IOException {
        Stage alertBox = new Stage();
        alertBox.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(AlertBoxController.class.getResource("AlertBox.fxml"));
        Parent root = loader.load();

        AlertBoxController alertBoxController = loader.getController();
        alertBoxController.setErrorMessage(msg);

        alertBox.setScene(new Scene(root));
        alertBox.showAndWait();
    }
}
