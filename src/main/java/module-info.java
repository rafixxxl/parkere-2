module com.example.parkere {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.parkere to javafx.fxml;
    exports com.example.parkere;
    exports com.example.parkere.controllers;
    opens com.example.parkere.controllers to javafx.fxml;
}